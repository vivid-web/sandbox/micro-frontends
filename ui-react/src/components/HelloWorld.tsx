import React from 'react';

export const HelloWorld = (): React.ReactElement => (
    <div>Hello from React UI!</div>
);

export default HelloWorld;
