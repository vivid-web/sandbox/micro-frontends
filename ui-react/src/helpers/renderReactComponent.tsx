import React, { ReactNode } from 'react'
import ReactDOM from 'react-dom/client'

export const renderReactComponent = (element: Element | DocumentFragment, Component: any) => ReactDOM.createRoot(element).render(
    <React.StrictMode>
        <Component />
    </React.StrictMode>
);
export default renderReactComponent
