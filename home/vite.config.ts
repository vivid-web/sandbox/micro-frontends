import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import federation from "@originjs/vite-plugin-federation";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    federation({
      name: "home",
      filename: "remoteEntry.js",
      remotes: {
        "ui_vue": {
          external: "http://localhost:3001/assets/remoteEntry.js",
          format: 'esm',
          from: 'vite'
        },
        "ui_react": {
          external: "http://localhost:3002/assets/remoteEntry.js",
          format: 'esm',
          from: 'vite'
        },
        "ui_svelte": {
          external: "http://localhost:3003/assets/remoteEntry.js",
          format: 'esm',
          from: 'vite'
        },
      },
      shared: ["vue"]
    }),
  ]
})
