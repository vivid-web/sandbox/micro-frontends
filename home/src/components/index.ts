import HelloWorldFromReact from './HelloWorldFromReact.vue';
import HelloWorldFromSvelte from './HelloWorldFromSvelte.vue';
import HelloWorldFromVue from './HelloWorldFromVue.vue';

export {
    HelloWorldFromReact,
    HelloWorldFromSvelte,
    HelloWorldFromVue,
};

export default {
    HelloWorldFromReact,
    HelloWorldFromSvelte,
    HelloWorldFromVue,
};
