import { createApp } from 'vue'
import { Component } from '@vue/runtime-core';

export const renderVueComponent = (element: any, Component: Component) => (
    createApp(Component).mount(element)
);

export default renderVueComponent;
