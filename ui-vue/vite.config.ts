import { defineConfig } from 'vite'
import federation from "@originjs/vite-plugin-federation";
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    federation({
      name: 'ui_vue',
      filename: 'remoteEntry.js',
      exposes: {
        './HelloWorld': './src/components/HelloWorld.vue',
        './renderVueComponent': './src/helpers/renderVueComponent',
      },
      shared: ['vue']
    })
  ],
  build: {
    target: 'esnext'
  }
})
