import { defineConfig } from 'vite'
import federation from "@originjs/vite-plugin-federation";
import { svelte } from '@sveltejs/vite-plugin-svelte'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
      svelte(),
      federation({
        name: 'ui_svelte',
        filename: 'remoteEntry.js',
        exposes: {
          './HelloWorld': './src/components/HelloWorld.svelte',
        }
      })
  ],
  build: {
    target: 'esnext'
  }
})
